<?php

require_once './vendor/autoload.php';

use app\characters\CharacterFactory;
use app\Ring;

$ring       = new Ring();
$orderus    = CharacterFactory::createOrderus();
$beast      = CharacterFactory::createBeast();
$ring->setHero($orderus);
$ring->setBeast($beast);

$ring->fight();
