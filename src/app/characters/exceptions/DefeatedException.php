<?php

namespace app\characters\exceptions;

/**
 * Class DefeatedException
 * @package app\characters\exceptions
 */
class DefeatedException extends \Exception { }
