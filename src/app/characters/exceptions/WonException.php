<?php

namespace app\characters\exceptions;
/**
 * Class WonException
 * @package app\characters\exceptions
 */
class WonException extends \Exception { }
