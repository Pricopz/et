<?php

namespace app\characters;

use app\characters\skills\MagicShieldSkill;
use app\characters\skills\RapidStrikeSkill;

/**
 * Class Orderus
 * @package app
 */
class Orderus extends Hero
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Orderus';
    }

    /**
     * @throws \Exception
     */
    public function init(): void
    {
        $this->registerSkills([new MagicShieldSkill(), new RapidStrikeSkill()]);
        $this->_health      = random_int(70, 100);
        $this->_strength    = random_int(70, 80);
        $this->_defence     = random_int(45, 55);
        $this->_speed       = random_int(40, 50);
        $this->_luck        = random_int(10, 30);
        $this->log("Player {$this->getName()} initialised.");
        $this->log("Health: {$this->_health}");
        $this->log("Strength: {$this->_strength}");
        $this->log("Defence: {$this->_defence}");
        $this->log("Speed: {$this->_speed}");
        $this->log("Luck: {$this->_luck}");
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function hasLuck(): bool
    {
        return random_int(0, 99) < $this->getLuck();
    }
}
