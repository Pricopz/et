<?php

namespace app\characters\skills;

use app\characters\Hero;

/**
 * Class RapidStrikeSkill
 * @package app\skills
 */
class RapidStrikeSkill implements ISkill
{

    public function useSkill(Hero $hero): void
    {
        $hero->setStrikePower($hero->getStrength() * 2);
    }

    public function isActive(): bool
    {
        return random_int(0, 99) < $this->getChance();
    }

    public function getType(): int
    {
        return SkillType::TYPE_ATTACK;
    }

    public function getChance(): int
    {
        return 10;
    }

    public function getName(): string
    {
        return 'Rapid Strike';
    }
}
