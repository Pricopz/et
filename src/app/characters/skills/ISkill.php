<?php

namespace app\characters\skills;

use app\characters\Hero;

/**
 * Class AbstractSkill
 * @package app\skills
 */
interface ISkill
{
    public function useSkill(Hero $hero): void;

    public function isActive(): bool;

    public function getType(): int;

    public function getChance(): int;

    public function getName(): string;
}
