<?php

namespace app\characters\skills;

use app\characters\Hero;

/**
 * Class MagicShieldSkill
 * @package app
 */
class MagicShieldSkill implements ISkill
{

    public function useSkill(Hero $hero): void
    {
        $hero->setDamage(round($hero->getDamage() / 2));
    }

    public function isActive(): bool
    {
        return random_int(0, 99) < $this->getChance();
    }

    public function getType(): int
    {
        return SkillType::TYPE_DEFENSE;
    }

    public function getChance(): int
    {
        return 20;
    }

    public function getName(): string
    {
        return 'Magic Shield';
    }
}
