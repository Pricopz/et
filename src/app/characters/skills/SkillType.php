<?php

namespace app\characters\skills;

/**
 * Class SkillType
 * @package app\skills
 */
abstract class SkillType
{
    public const TYPE_ATTACK    = 1;
    public const TYPE_DEFENSE   = 2;
}
