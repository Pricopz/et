<?php

namespace app\characters;

/**
 * Class CharacterFactory
 * @package app
 */
class CharacterFactory
{
    public static function createOrderus(): Orderus
    {
        $orderus = new Orderus();
        $orderus->init();
        return $orderus;
    }

    public static function createBeast(): Beast
    {
        $beast = new Beast();
        $beast->init();
        return $beast;
    }
}
