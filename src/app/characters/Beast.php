<?php

namespace app\characters;

/**
 * Class Beast
 * @package app
 */
class Beast extends AbstractCharacter
{

    public function getName(): string
    {
        return 'Wild Beast';
    }

    public function init(): void
    {
        $this->_health      = random_int(60, 90);
        $this->_strength    = random_int(60, 90);
        $this->_defence     = random_int(40, 60);
        $this->_speed       = random_int(40, 60);
        $this->_luck        = random_int(25, 40);

        $this->log("Player {$this->getName()} initialised.");
        $this->log("Health: {$this->_health}");
        $this->log("Strength: {$this->_strength}");
        $this->log("Defence: {$this->_defence}");
        $this->log("Speed: {$this->_speed}");
        $this->log("Luck: {$this->_luck}");
    }

    public function hasLuck(): bool
    {
        return random_int(0, 99) < $this->getLuck();
    }
}
