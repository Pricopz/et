<?php

namespace app\characters;

use app\characters\skills\ISkill;
use app\characters\skills\SkillType;

/**
 * Class Hero
 * @package app
 */
abstract class Hero extends AbstractCharacter
{
    /**
     * @var int
     */
    protected int $_strikePower;

    /**
     * @return int
     */
    public function getStrikePower(): int
    {
        return $this->_strikePower;
    }

    /**
     * @param int $strikePower
     */
    public function setStrikePower(int $strikePower): void
    {
        $this->_strikePower = $strikePower;
    }

    /**
     * @var ISkill[]
     */
    protected array $_attackSkills = [];
    /**
     * @var ISkill[]
     */
    protected array $_defenseSkills = [];

    /**
     * @param ISkill[] $skills
     */
    public function registerSkills(array $skills): void
    {
        foreach ($skills as $skill) {
            $this->registerSkill($skill);
        }
    }

    /**
     * @param ISkill $skill
     */
    public function registerSkill(ISkill $skill): void
    {
        if ($skill->getType() === SkillType::TYPE_ATTACK) {
            $this->_attackSkills[] = $skill;
        } elseif ($skill->getType() === SkillType::TYPE_DEFENSE) {
            $this->_defenseSkills[] = $skill;
        }
    }

    public function resetSkills(): void
    {
        $this->_attackSkills = [];
        $this->_defenseSkills = [];
    }

    public function strike(): int
    {
        $this->_strikePower = parent::strike();
        foreach ($this->_attackSkills as $attackSkill) {
            if ($attackSkill->isActive()) {
                $this->log("Skill '{$attackSkill->getName()}' used.");
                $attackSkill->useSkill($this);
            }
        }
        return $this->_strikePower;
    }

    public function defendFrom(int $attackStrength): void
    {
        parent::defendFrom($attackStrength);

        foreach ($this->_defenseSkills as $defenseSkill) {
            if ($defenseSkill->isActive()) {
                $this->log("Skill '{$defenseSkill->getName()}' used.");
                $defenseSkill->useSkill($this);
            }
        }
    }
}
