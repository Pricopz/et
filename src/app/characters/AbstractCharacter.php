<?php

namespace app\characters;

use app\characters\exceptions\WonException;
use app\LogTrait;

/**
 * Interface ICharacter
 * @package app
 */
abstract class AbstractCharacter
{
    use LogTrait;
    /**
     * @var int
     */
    protected int $_strength;
    /**
     * @var int
     */
    protected int $_defence;
    /**
     * @var int
     */
    protected int $_speed;
    /**
     * @var int
     */
    protected int $_luck;

    /**
     * @param int $luck
     */
    public function setLuck(int $luck): void
    {
        $this->_luck = $luck;
    }
    /**
     * @var int
     */
    protected int $_damage = 0;

    /**
     * @var int
     */
    protected int $_health;

    /**
     * @param int $health
     */
    public function setHealth(int $health): void
    {
        $this->_health = $health;
    }
    /**
     * @param int $strength
     */
    public function setStrength(int $strength): void
    {
        $this->_strength = $strength;
    }
    /**
     * @param int $defence
     */
    public function setDefence(int $defence): void
    {
        $this->_defence = $defence;
    }
    /**
     * @param int $speed
     */
    public function setSpeed(int $speed): void
    {
        $this->_speed = $speed;
    }
    /**
     * @param int $damage
     */
    public function setDamage(int $damage): void
    {
        $this->_damage = $damage;
    }

    /**
     * @return int
     */
    public function getDamage(): int
    {
        return $this->_damage;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->_health;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->_strength;
    }

    /**
     * @return int
     */
    public function getDefence(): int
    {
        return $this->_defence;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->_speed;
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->_luck;
    }

    /**
     * @return bool
     */
    public function isDead(): bool
    {
        return $this->_health <= 0;
    }

    abstract public function getName(): string;

    abstract public function hasLuck(): bool;

    abstract public function init(): void;

    /**
     * @param int $damage
     */
    public function weakenedWith(int $damage): void
    {
        $this->log("Damage for {$this->getName()}: {$damage}");
        $this->_health -= $damage;
        $this->log("Health status for {$this->getName()}: {$this->_health}");
    }

    /**
     * @param AbstractCharacter $opponent
     * @throws WonException
     */
    public function fightWith(AbstractCharacter $opponent): void
    {
        if ($opponent->hasLuck()) {
            $this->log("Player '{$opponent->getName()}' had Luck.");
            return;
        }

        $opponent->defendFrom($this->strike());

        $opponent->weakenedWith($this->_damage);

        if ($opponent->isDead()) {
            throw new WonException("Player '{$this->getName()}' won the battle!");
        }
    }

    public function strike(): int
    {
        $this->log("{$this->getName()} strike.");
        return $this->_strength;
    }

    public function defendFrom(int $strikePower): void
    {
        $this->log("{$this->getName()} defend.");
        $this->_damage = $strikePower - $this->_defence;
    }
}
