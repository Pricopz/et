<?php

use app\characters\Beast;
use app\characters\exceptions\WonException;
use app\characters\Orderus;
use PHPUnit\Framework\TestCase;

require_once  __DIR__ .  '/../../vendor/autoload.php';


class OrderusTest extends TestCase
{
    public function testGetName(): void
    {
        $orderus = new Orderus();
        $this->assertEquals('Orderus', $orderus->getName());
    }

    public function testHasLuckTrue(): void
    {
        $orderus = new Orderus();
        $orderus->setLuck(100);
        $this->assertTrue($orderus->hasLuck());
    }

    public function testHasLuckFalse(): void
    {
        $orderus = new Orderus();
        $orderus->setLuck(0);
        $this->assertFalse($orderus->hasLuck());
    }

    //
    public function testWeakenedWith(): void
    {
        $orderus = new Orderus();
        $orderus->setLog(false);
        $orderus->setHealth(90);
        $orderus->weakenedWith(20);
        $this->assertEquals(70, $orderus->getHealth());
    }

    public function testStrike(): void
    {
        $orderus = new Orderus();
        $orderus->setLog(false);
        $orderus->setStrength(90);
        $s = $orderus->strike();
        $this->assertEquals(90, $s);
    }

    public function testFightWithWhenOpponentHasLuck(): void
    {
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getMockBuilder(Beast::class)->onlyMethods(['hasLuck', 'defendFrom', 'weakenedWith'])->getMock();
        $beastMock->method('hasLuck')->willReturn(true);

        $beastMock->expects($this->once())->method('hasLuck');
        $beastMock->expects($this->never())->method('defendFrom');
        $beastMock->expects($this->never())->method('weakenedWith');

        $orderus = new Orderus();
        $orderus->setLog(false);
        $orderus->fightWith($beastMock);
    }


    public function testFightWithWhenOpponentHasNoLuck(): void
    {
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getMockBuilder(Beast::class)->onlyMethods(['hasLuck', 'defendFrom', 'weakenedWith', 'isDead'])->getMock();
        $beastMock->method('hasLuck')->willReturn(false);
        $beastMock->method('isDead')->willReturn(false);


        $beastMock->expects($this->once())->method('hasLuck');
        $beastMock->expects($this->once())->method('defendFrom');
        $beastMock->expects($this->once())->method('weakenedWith');
        $beastMock->expects($this->once())->method('isDead');

        $orderus = new Orderus();
        $orderus->setLog(false);
        $orderus->setStrength(70);
        $orderus->fightWith($beastMock);
    }


    public function testFightWithWhenOpponentHasNoLuckAndDie(): void
    {
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getMockBuilder(Beast::class)->onlyMethods(['hasLuck', 'defendFrom', 'weakenedWith', 'isDead'])->getMock();
        $beastMock->method('hasLuck')->willReturn(false);
        $beastMock->method('isDead')->willReturn(true);


        $beastMock->expects($this->once())->method('hasLuck');
        $beastMock->expects($this->once())->method('defendFrom');
        $beastMock->expects($this->once())->method('weakenedWith');
        $beastMock->expects($this->once())->method('isDead');
        $this->expectException(WonException::class);

        $orderus = new Orderus();
        $orderus->setLog(false);
        $orderus->setStrength(70);
        $orderus->fightWith($beastMock);

    }


    private function getBeastMock()
    {
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getMockBuilder(Beast::class)->onlyMethods(['hasLuck'])->getMock();
        $beastMock->method('hasLuck')->willReturn(true);
        return $beastMock;
    }
}
