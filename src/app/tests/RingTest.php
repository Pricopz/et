<?php

require_once  __DIR__ .  '/../../vendor/autoload.php';

use app\characters\Beast;
use app\characters\Hero;
use app\characters\Orderus;
use PHPUnit\Framework\TestCase;
use app\Ring;

/**
 * Class RingTest
 */
class RingTest extends TestCase
{
    public function testIsHeroFirstTrueWhenHeroHasGreaterSpeed(): void
    {
        /**
         * @var $heroMock Hero
         */
        $heroMock = $this->getHeroMock(90, 70);
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getBeastMock(70, 70);

        $ring = new Ring();
        $ring->setHero($heroMock);
        $ring->setBeast($beastMock);
        $isHeroFirst = $ring->isHeroFirst();
        $this->assertTrue($isHeroFirst);
    }

    public function testIsHeroFirstTrueWhenHeroHasGreaterLuck(): void
    {
        /**
         * @var $heroMock Hero
         */
        $heroMock = $this->getHeroMock(70, 90);
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getBeastMock(70, 70);

        $ring = new Ring();
        $ring->setHero($heroMock);
        $ring->setBeast($beastMock);
        $isHeroFirst = $ring->isHeroFirst();
        $this->assertTrue($isHeroFirst);
    }

    public function testIsHeroFirstFalseWhenHeroHasLessSpeedAndLuck(): void
    {
        /**
         * @var $heroMock Hero
         */
        $heroMock = $this->getHeroMock(60, 60);
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getBeastMock(70, 70);

        $ring = new Ring();
        $ring->setHero($heroMock);
        $ring->setBeast($beastMock);
        $isHeroFirst = $ring->isHeroFirst();
        $this->assertFalse($isHeroFirst);
    }

    public function testGetPlayersWhenHeroIsFirst(): void
    {
        /**
         * @var $heroMock Hero
         */
        $heroMock = $this->getHeroMock(80, 80);
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getBeastMock(70, 70);

        $ring = new Ring();
        $ring->setHero($heroMock);
        $ring->setBeast($beastMock);
        $players = $ring->getPlayers($ring->isHeroFirst());
        $this->assertInstanceOf(Hero::class, $players[0]);
        $this->assertInstanceOf(Beast::class, $players[1]);
    }

    public function testGetPlayersWhenHeroIsSecond(): void
    {
        /**
         * @var $heroMock Hero
         */
        $heroMock = $this->getHeroMock(60, 60);
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getBeastMock(70, 70);

        $ring = new Ring();
        $ring->setHero($heroMock);
        $ring->setBeast($beastMock);
        $players = $ring->getPlayers($ring->isHeroFirst());
        $this->assertInstanceOf(Beast::class, $players[0]);
        $this->assertInstanceOf(Hero::class, $players[1]);
    }

    /**
     * @param int $speed
     * @param int $luck
     * @return Hero
     */
    private function getHeroMock(int $speed, int $luck)
    {
        /**
         * @var $heroMock Hero
         */
        $heroMock = $this->getMockBuilder(Orderus::class)->onlyMethods(['getSpeed', 'getLuck'])->getMock();
        $heroMock->method('getSpeed')->willReturn($speed);
        $heroMock->method('getLuck')->willReturn($luck);
        return $heroMock;
    }

    /**
     * @param int $speed
     * @param int $luck
     * @return Beast
     */
    private function getBeastMock(int $speed, int $luck)
    {
        /**
         * @var $beastMock Beast
         */
        $beastMock = $this->getMockBuilder(Beast::class)->onlyMethods(['getSpeed', 'getLuck'])->getMock();
        $beastMock->method('getSpeed')->willReturn($speed);
        $beastMock->method('getLuck')->willReturn($luck);
        return $beastMock;
    }

    public function testGetStrikerIndex(): void
    {
        $ring = new Ring();
        $strikerIndex = $ring->getStrikerIndex(1);
        $defenderIndex = $ring->getDefenderIndex(2);

        $this->assertEquals(0, $strikerIndex);
        $this->assertEquals(0, $defenderIndex);
    }
}
