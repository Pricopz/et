<?php

require_once  __DIR__ .  '/../../vendor/autoload.php';

use app\characters\Orderus;
use app\characters\skills\MagicShieldSkill;
use app\characters\skills\SkillType;
use PHPUnit\Framework\TestCase;

class MagicShieldSkillTest extends TestCase
{

    public function testGetName(): void
    {
        $skill = new MagicShieldSkill();
        $this->assertEquals('Magic Shield', $skill->getName());
    }

    public function testGetChange(): void
    {
        $skill = new MagicShieldSkill();
        $this->assertEquals(20, $skill->getChance());
    }

    public function testGetType(): void
    {
        $skill = new MagicShieldSkill();
        $this->assertEquals(SkillType::TYPE_DEFENSE, $skill->getType());
    }

    public function testUseSkill(): void
    {
        $hero = new Orderus();
        $hero->setLog(false);
        $hero->setDamage(70);
        $skill = new MagicShieldSkill();
        $skill->useSkill($hero);
        $this->assertEquals(35, $hero->getDamage());
    }
}
