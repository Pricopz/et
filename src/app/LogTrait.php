<?php

namespace app;

/**
 * Trait LogTrait
 * @package app
 */
trait LogTrait
{
    /**
     * @var bool
     */
    protected bool $_log = true;

    /**
     * @param bool $log
     */
    public function setLog(bool $log): void
    {
        $this->_log = $log;
    }

    /**
     * @param string $message
     */
    protected function log(string $message): void
    {
        if ($this->_log) {
            echo $message . PHP_EOL;
        }
    }
}
