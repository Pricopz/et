<?php

namespace app;

use app\characters\exceptions\WonException;
use app\characters\Hero;
use app\characters\Beast;
use app\characters\AbstractCharacter;

/**
 * Class Ring
 * @package app
 */
class Ring
{
    use LogTrait;
    /**
     * Max Rounds
     */
    public const MAX_ROUNDS = 20;
    /**
     * @var Hero
     */
    protected Hero $_hero;
    /**
     * @var Beast
     */
    protected Beast $_beast;

    /**
     * @param Hero $hero
     */
    public function setHero(Hero $hero): void
    {
        $this->_hero = $hero;
    }

    /**
     * @param Beast $beast
     */
    public function setBeast(Beast $beast): void
    {
        $this->_beast = $beast;
    }

    /**
     * @return bool
     */
    public function isHeroFirst(): bool
    {
        return $this->_hero->getSpeed() > $this->_beast->getSpeed() ||
            $this->_hero->getLuck() > $this->_beast->getLuck();
    }

    /**
     * @param bool $heroFirst
     * @return array
     */
    public function getPlayers(bool $heroFirst): array
    {
        return $heroFirst ? [$this->_hero, $this->_beast] : [$this->_beast, $this->_hero];
    }

    /**
     * @param int $round
     * @return int
     */
    public function getStrikerIndex(int $round): int
    {
        return abs(($round % 2) - 1);
    }

    /**
     * @param int $round
     * @return int
     */
    public function getDefenderIndex(int $round): int
    {
        return $round % 2;
    }

    public function fight(): void
    {
        $round = 1;
        /**
         * @var $players AbstractCharacter[]
         */
        $players = $this->getPlayers($this->isHeroFirst());
        $this->log("First player: {$players[0]->getName()}");
        $this->log("Second player: {$players[1]->getName()}");

        try {
            while ($round < self::MAX_ROUNDS) {
                $this->log("Round {$round} started.");

                $strikerIndex   = $this->getStrikerIndex($round);
                $defenderIndex  = $this->getDefenderIndex($round);

                $players[$strikerIndex]->fightWith($players[$defenderIndex]);

                $this->log(' ');
                $round++;
            }
        } catch (WonException $e) {
            $this->log("Fight Ended at round {$round}");
            echo $e->getMessage();
        }
    }
}
