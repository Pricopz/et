### Install app
- get project using command `git clone https://Pricopz@bitbucket.org/Pricopz/et.git`
- run command `cd src`
- run command `composer install`


### Run app
- navigate to the application root
- run command `docker-compose up`
- run command `dokcer ps` and get the container ID
- run command `docker exec -it [container ID] bash`
- run command `php index.php`

### Tests
- navigate to the application root
- run command `docker-compose up`
- run command `dokcer ps` and get the container ID
- run command `docker exec -it [container ID] bash`
- run command `vendor/bin/phpunit app/tests`
